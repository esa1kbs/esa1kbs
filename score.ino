/************************************************************************/
/*	score.ino       KBS Microcontrollers								*/
/*																		*/
/*	All code regarding score and high scores							*/
/*																		*/
/************************************************************************/


/************************************************************************/
/*	Function: CalculateScore											*/
/*  ------------------------											*/
/*	Calculates the score of the player with the use of the time			*/
/*	left and the lives left of the player.								*/
/*																		*/
/*	timeLeft:	integer with in-game time left							*/
/*	lives:		char with players lives left							*/
/*																		*/
/*	returns: integer with calculated score								*/
/************************************************************************/
int CalculateScore(int timeLeft, char lives)
{
	uint8_t Score;
	double TimeScore;
	uint8_t LivesScore;
	
	// calculate score time
	TimeScore = (timeLeft/framerate) * 1,5;
	// calculate score lives
	LivesScore = lives * 20;
	floor(TimeScore);
	// Total score
	Score = TimeScore + LivesScore;

	return Score;
}


/************************************************************************/
/*	Function: setHighscore												*/
/*  ----------------------												*/
/*	The name and the score of the player are written to the EEPROM.		*/
/*																		*/
/*	place:	8-bit unsigned int which contains...						*/
/*	l1:		char which contains...										*/
/*	l2:		char which contains...										*/
/*	l3:		char which contains...										*/
/*	score:	8-bit unsigned int which contains the score					*/
/*																		*/
/*	returns: integer with calculated score								*/
/************************************************************************/
void setHighscore(int8_t place, char l1,char l2,char l3, int8_t score)
{
	char p = 0;
	// Read data that can get moved if better highscore.
	char p11 = EEPROM.read(0);
	char p12 = EEPROM.read(1);
	char p13 = EEPROM.read(2);
	uint8_t p1score = checkHighscore(0);
	
	char p21 = EEPROM.read(4);
	char p22 = EEPROM.read(5);
	char p23 = EEPROM.read(6);
	uint8_t p2score = checkHighscore(4);

	if (place == 1)
	{
		// Set former player one to second place
		WriteToEEPROM(p+4,p11,p12,p13,p1score);
		// Set former player two to third place
		WriteToEEPROM(p+8,p21,p22,p23,p2score);
		// Set new score to first place.
		WriteToEEPROM(p, l1, l2, l3, score);
	}
	if (place == 2)
	{
		// Set former player two to third place
		WriteToEEPROM(p+8,p21,p22,p23,p2score);
		WriteToEEPROM(p+4, l1, l2, l3, score);
	}
	if (place == 3)
	{
		WriteToEEPROM(p + 8, l1, l2, l3, score);
	}
	if (place == 4)
	{
		 // Do nothing
	}
}


/************************************************************************/
/*	Function: checkIfHighscore											*/
/*  --------------------------											*/
/*	Checks if the incoming score of the player is a highscore.			*/
/*																		*/
/*	incscore:	integer which contains score							*/
/*																		*/
/*	returns: 8-bit unsigned integer with position in high scores		*/
/************************************************************************/
int8_t checkIfHigscore(int incscore)
{
	uint8_t score1 = EEPROM.read(3);
	uint8_t score2 = EEPROM.read(7);
	uint8_t score3 = EEPROM.read(11);

	if (incscore >= score1)
	{
		return 1;
	}
	else if (incscore >= score2)
	{
		return 2;
	}
	else if (incscore >= score3)
	{
		return 3;
	}
	else
	{
		return 4;
	}
}


/************************************************************************/
/*	Function: WriteToEEPROM												*/
/*  -----------------------												*/
/*	Writes the data to the correct place in the EEPROM.					*/
/*																		*/
/*	p:			integer which contains position							*/
/*	letter1:	char which contains...									*/
/*	letter2:	char which contains...									*/
/*	letter3:	char which contains...									*/
/*	highscore:	8-bit unsigned integer which contains...				*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void WriteToEEPROM(int p, char letter1, char letter2, char letter3, uint8_t highscore)
{
	// writes letters to EEPROM
	EEPROM.write(p, letter1);
	EEPROM.write(p+1, letter2);
	EEPROM.write(p+2, letter3);
	// writes score to EEPROM
	EEPROM.write(p+3, highscore);
}


/************************************************************************/
/*	Function: checkHighscore											*/
/*  ------------------------											*/
/*	Reads the highscore of the given place.								*/
/*																		*/
/*	p:	integer which contains position									*/
/*																		*/
/*	returns: 8-bit unsigned integer with selected high score			*/
/************************************************************************/
uint8_t checkHighscore(int p)
{
	uint8_t highscore;
	highscore = EEPROM.read(p+3); 
	return highscore;
}


/************************************************************************/
/*	Function: ClearEEPROM												*/
/*  ---------------------												*/
/*	Clears the part of the EEPROM where the highscores are.				*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void ClearEEPROM()
{
	for (int i = 0 ; i < 20 ; i++) 
	{
		EEPROM.write(i, 0);
	}
}
