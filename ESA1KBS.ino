/************************************************************************/
/*	ESA1KBS.ino       KBS Microcontrollers								*/
/* 																		*/
/*	Game states: 														*/
/*	gameState 1: Main screen											*/
/*	gameState 2: Connecting												*/
/*	gameState 3: Countdown												*/
/*	gameState 4: The game												*/
/*	gameState 5: End game screen / sending data							*/
/*	gameState 6: Score screen											*/
/*	gameState 7: Name screen											*/
/*	gameState 8: Level screen											*/
/*	gameState 9: HighScore screen										*/
/*																		*/
/*	Positions:															*/
/*	0: empty															*/
/*	1: obstacle															*/
/*	2: wall																*/
/*	3: bomb																*/
/*	4: explosion effect													*/
/*																		*/
/************************************************************************/

#define WIDTH 11
#define HEIGHT 7

#include <avr/io.h>
#include <ArduinoNunchuk.h>
#include "GraphicsLib.h"
#include "MI0283QT9.h"
#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>

#include <SPI.h>

struct object
{
	char id;
	char x;
	char y;
	char age; // For bombs only
	char owner; // For bombs only
};

MI0283QT9 display;
ArduinoNunchuk nunchuk = ArduinoNunchuk();

int serialNumber = 0; // For checking if messages are skipped
int lastNumber = 0;

int topy, width, height, countWidth, blockWidth, countHeight, waitToMove;
int poppetje[13];

char gameState = 0;

byte errorCounter = 1;

char Iam = 1; // Which player? 1 or 2 (temporarily)
char otherIs = 2;

volatile uint8_t counter = 0; // Counter for calculating when tick must be executed
char framerate = 10; // FPS
char playerTimeSecs = 120; // Number of seconds a game should last
int ticksleft = playerTimeSecs * framerate; // In ticks
volatile char dotick = 0; // Holds value if tick has to be executed

const char bombsArraySize = 14; // Holds all bombs on the screen
const char bombeffectsArraySize = 60; // Holds all bomb effects on the screen
struct object bombs[bombsArraySize];
struct object bombeffects[bombeffectsArraySize];
char bombIdCounter = 0; // Holds current position in array of bombs
char bombeffectIdCounter = 0; // Holds current position in array of bomb effects
char ownBombLimit = 2; // Bomb limit
char otherBombLimit = 2; // Bomb limit other player

char ownLives = 3; // Counts lives left
char otherLives = 3; // Counts lives other player left

char ownBombDistance = 3; // Bomb distance
char otherBombDistance = 3; // Bomb distance other player

char ownRunningSpeed = 1; // Speed running (1 is fast, higher is slower ;))
char otherRunningSpeed = 1; // Speed other player

char checkX, checkY; // For counting distance bomb explosion

char gameLevel = 1;

char connected = 0;

// Chars for entering name screen
char save = 0;
char letters[26] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
char i = 0, j = 0, y = 0;
char Highscoretimer = 2;
int TimeLeft;
int EndScorePlayer1;
int EndScorePlayer2;
char HighScoreBoolean;

// Variables for IR communication
char isReceiving = 0;
volatile uint8_t irCounter = 0;
char posCounter = 0;
uint8_t byteToSend;
char pinHigh = 0;

byte fieldSeed;

// Reserve some space for both player objects
struct object *player1 = (struct object*)malloc(sizeof(struct object));
struct object *player2 = (struct object*)malloc(sizeof(struct object));

void initiateScreen(char field[][HEIGHT]);
void generateField(char field[][HEIGHT]);

int main(void) {
	init();
	
	USART_init(9600);
	//Serial.begin(9600);
	
	nunchuk.init();
	display.begin(16);
	
	//ADCsingleREAD()
	
	// Create random seed for random() function
	nunchuk.update();
	randomSeed(nunchuk.accelX*nunchuk.accelY/nunchuk.accelZ);

	// Reserve some space on top of screen for stats
	topy = 37;
	
	width = display.getWidth(); // Screen width
	height = display.getHeight()-topy; // Screen height minus space for stats
	
	countWidth = WIDTH; // Number of horizontal blocks
	blockWidth = floor(width / countWidth); // Width of blocks by dividing horizontal space by number of blocks
	countHeight = floor(height / blockWidth);  // Same for height
	
	display.touchStartCal();
	DrawMainScreen(); // Draw the main screen
	gameState = 1; // Set game state to main menu
	
	// Initiate two-dimensional array which saves playing field data
	char field[WIDTH][HEIGHT];
	
	DDRC |= 0x7;
	
	setupTimers();
	
	// Used for drawing sprites:
	poppetje[0]=blockWidth/20;
	poppetje[1]=blockWidth/10;
	poppetje[2]=12*blockWidth/100;
	poppetje[3]=15*blockWidth/100;
	poppetje[4]=22*blockWidth/100;
	poppetje[5]=33*blockWidth/100;
	poppetje[6]=35*blockWidth/100;
	poppetje[7]=40*blockWidth/100;
	poppetje[8]=50*blockWidth/100;
	poppetje[9]=66*blockWidth/100;
	poppetje[10]=70*blockWidth/100;
	poppetje[11]=75*blockWidth/100;
	poppetje[12]=85*blockWidth/100;

	waitToMove = 0;
	
	while(1) {
		if(dotick)
		{
			dotick = 0;
			
			// Read incomming data
			readData(player2, field);
			
			//10hz
			// Read NunChuck data
			nunchuk.update();
				
			// Read nunchuk
			char winddirection = calculateMovement(player1, field);
			
			if(gameState == 1) // Main menu
			{
				// Read touchscreen
				if(display.touchRead()){
					// if "Play" is touched
					if(display.touchX() > 97 && display.touchX() < 212 && display.touchY() > 57 && display.touchY() < 85) // Start game
					{
						/*gameLevel = 1;*/
						gameState = 2; // Set game state to 2 (playing screen)
						startConnecting();
						
					}
					// If "Levels is touched"
					if(display.touchX() > 80 && display.touchX() < 220 && display.touchY() > 97 && display.touchY() < 125){
					 gameState = 8; // Set game state to 8 (LevelScreen)
					 LevelScreen();
					
						
					}
					// If "Highscore" is touched
					if (display.touchX() > 43 && display.touchX() < 258 && display.touchY() > 137 && display.touchY() < 165){
						gameState  = 9; // Set game state to 9 (HighScoreScreen)
						HighScoreScreen();
					}
				}
				
			}
			else if(gameState == 2) // Connecting
			{
				
				if(ticksleft < 58 * framerate)
				{
					// I am the boss
					// Send AUA
					byte byte1 = 0xF << 4; // Send F code (AUA)
					byte1 |= (serialNumber<<2); // 2 bits from serial number
					USART_putch(byte1);
					serialNumber++;
					if(serialNumber == 4) serialNumber = 0;

				} else
				{
					// The other is the boss
					// Listen for AUA (in readData() function, not handled here)
				}

				if(ticksleft <= 0)
				{
					gameState = 1;
					DrawMainScreen(); // Draw the main screen
				}
				
				if (connected){
					gameState = 3;

					player1->id = Iam;
					player2->id = otherIs;
					
					if(Iam == 1)
					{
						player1->x = 0;
						player1->y = 0;
						player2->x = WIDTH-1;
						player2->y = HEIGHT-1;
					}
					else
					{
						player1->x = WIDTH-1;
						player1->y = HEIGHT-1;
						player2->x = 0;
						player2->y = 0;
					}
					startVar(); // Setup player positions and game values
					
					display.fillScreen(RGB(0,0,0));
					generateField(field, fieldSeed); // Generate field
					initiateScreen(field); // Draw playing field
					drawObject(player1, 0, field);
					drawObject(player2, 0, field);
					ticksleft = 3*framerate; // 3 sec countdown before start game.
				}
				
			}
			else if (gameState == 3) // Start game countdown
			{
				display.drawInteger(150,100,ticksleft / framerate,10,RGB(255,255,255),RGB(0,0,0),5);
				
				if (ticksleft < 0)
				{
					// redraw positions at place where timer was
					fillPosition(field,5,2);
					fillPosition(field,5,3);
					fillPosition(field,6,2);
					fillPosition(field,6,3);
					ticksleft = playerTimeSecs * framerate; // In ticks
					gameState = 4;
				}

			}
			else if(gameState == 4) // Game
			{
				// Make player flashing first 3 seconds
				if(ticksleft > (playerTimeSecs - 3) * framerate) // 3 seconds
				{
					char drawCharacter = (playerTimeSecs * 2)%2; // Make own character flashing. 1 = every second, 2 = every half second, 3 = every third second, etc. (don't change %2 !!!)
					drawObject(player1, drawCharacter, field); // Draw player with corresponding 0 or 1 to make it flash.
				}
				
				// Update the time and draw
				updateTime(ticksleft, framerate);
				
				// Move player
				switch (winddirection)
				{
					case 'N':
					moveObject(player1, 0, -1, field); // Lower Y
					break;
					case 'O':
					moveObject(player1, 1, 0, field); // Rise X
					break;
					case 'Z':
					moveObject(player1, 0, 1, field); // Rise Y
					break;
					case 'W':
					moveObject(player1, -1, 0, field); // Lower X
					break;
				}
				
				// Check if player wants to drop bomb
				if(nunchuk.zButton)
				{
					nunchuk.zButton = 0;
					
					dropBomb(player1,player1->x,player1->y, field);
					
				}
				
				// Check for bomb updates
				checkForBombUpdates(player1, player2, field);
				
				// Check for bomb effects updates
				checkForBombeffectsUpdates(player1, player2, field);
				
				// Check for game time
				if(ticksleft <= 0)
				{
					gameState = 5; // After game screen
					
					// Send score
					sendScore();
					
					EndGameScreen();
					ticksleft = 3*framerate; // 3 sec countdown before next gameState.
					
				}
				
				// Update lives display
				updateLives();

				// Check for lives
				if (ownLives <= 0){
					
					// Send death message to other player
					for(int i = 0; i<200; i++)
					{
						byte byte1 = 0x9 << 4;
						byte1 |= serialNumber<<2;
						serialNumber++; // Increase serial number and check if it can be reset
						if(serialNumber == 4) serialNumber = 0;
						byte1 |= 0x03; // Checksum is 11 (hex 3)
						USART_putch(byte1);
					}
					// Continue to next game state
					TimeLeft = ticksleft;
					
					// Send score
					sendScore();
					
					gameState = 5;
					EndGameScreen();
					ticksleft = 3*framerate; // 3 sec countdown before next gameState.
				}
			}
			else if(gameState == 5)
			{
				if (ticksleft <= 0)
				{
					gameState = 6;
					
					HighScoreBoolean = checkIfHigscore(EndScorePlayer1); // is highscore
					// Draws score screen
					ScoreScreen(EndScorePlayer1, EndScorePlayer2, HighScoreBoolean);

				}
			}
			else if (gameState == 6)
			{
				if(display.touchRead())
				{
					// if "HIGHSCORE" is touched
					if(display.touchX() > 18 && display.touchX() < 218 && display.touchY() > 185 && display.touchY() < 220) // button in score screen
					{
						if(HighScoreBoolean == 1 || HighScoreBoolean == 2 || HighScoreBoolean == 3 ) // if highscore is reached 
						{
							gameState  = 7; // Set game state to 8 (HighScoreScreen)
							GetNameScreen();
							startVar(); // resets all data
						}
						else 
						{ 
							startVar(); // resets all data
							gameState = 1;
							DrawMainScreen(); // Draw the main screen
							
						}

					}
				}
				
			} 
			else if (gameState == 7)
			{

				if(Highscoretimer == 2)
				{
					// Gets letter from array
					char letter1 = letters[i];
					char letter2 = letters[j];
					char letter3 = letters[y];
					
					//draws the letters indecated by the numbers 0 t/m 26 
					display.drawChar(62,84,letter1,RGB(0,0,0),RGB(255,255,255),4);
					display.drawChar(142,84,letter2,RGB(0,0,0),RGB(255,255,255),4);
					display.drawChar(222,84,letter3,RGB(0,0,0),RGB(255,255,255),4);
					
					if(display.touchRead())
					{	
						// first letter
						// UP
						if(display.touchX() > 45 && display.touchX() < 105 && display.touchY() > 36 && display.touchY() < 70) 
						{
							if(i == 0)
							{
								i = 26;
							}
							else
							{
								i--;
							}
						}
						//DOWN
						if(display.touchX() > 45 && display.touchX() < 105 && display.touchY() > 131 && display.touchY() < 165) 
						{
							if(i == 26)
							{
								i = 0;
							}
							else
							{
								i++;
							}
							
				
						}
			
						// second letter.
						// UP
						if(display.touchX() > 125 && display.touchX() < 185 && display.touchY() > 36 && display.touchY() < 70) 
						{
							if(j == 0)
							{
								j = 26;
							}
							else
							{
								j--;
							}
						}
						// DOWN
						if(display.touchX() > 125 && display.touchX() < 185 && display.touchY() > 131 && display.touchY() < 165) 
						{
							if(j == 26)
							{
								j = 0;
							}
							else
							{
								j++;
							}
							
						}

						// third letter
						// UP
						if(display.touchX() > 205 && display.touchX() < 265 && display.touchY() > 36 && display.touchY() < 70) 
						{
							if(y == 0)
							{
								y = 26;
							}
							else
							{
								y--;
							}
						}
						// DOWN
						if(display.touchX() > 205 && display.touchX() < 265 && display.touchY() > 131 && display.touchY() < 165) 
						{
							if(y == 26)
							{
								y = 0;
							}
							else
							{
								y++;
							}	
						}

						// pressed save button
						if(display.touchX() > 100 && display.touchX() < 210 && display.touchY() > 195 && display.touchY() < 230)
						{
							setHighscore(HighScoreBoolean, letter1,letter2, letter3, EndScorePlayer1);
							gameState = 9;
							HighScoreScreen();
						}
					}
				}

				Highscoretimer++;

				if (Highscoretimer > 2)
				{
					Highscoretimer = 0;
				}

			}
			else if (gameState == 8)
			{
				if(display.touchRead())
				{
					if(display.touchX() > 70 && display.touchX() < 220 && display.touchY() > 47 && display.touchY() < 75)
					{
						gameLevel = 1; // Set level to 1
						gameState = 2; // Starts game and chosen level
					
						// Send level
						byte byte1 = 0x7 << 4; // Send 7 code (send level)
						byte1 |= (serialNumber<<2); // 2 bits from serial number
					
						serialNumber++; // Update counter
						if(serialNumber == 4) serialNumber = 0;
					
						byte checkSum = gameLevel%4;
						byte1 |= (checkSum); // Add checksum to byte
						Serial.write(byte1);
					
						byte byte2 = gameLevel;
						USART_putch(byte2);

						startConnecting();
					
					}
					if(display.touchX() > 70 && display.touchX() < 220 && display.touchY() > 77 && display.touchY() < 105)
					{
						gameLevel = 2; // Set level to 2
						gameState = 2; // Starts game and chosen level
					
						// Send level
						byte byte1 = 0x7 << 4; // Send 7 code (send level)
						byte1 |= (serialNumber<<2); // 2 bits from serial number
					
						serialNumber++; // Update counter
						if(serialNumber == 4) serialNumber = 0;
					
						byte checkSum = gameLevel%4;
						byte1 |= (checkSum); // Add checksum to byte
						USART_putch(byte1);
					
						byte byte2 = gameLevel;
						USART_putch(byte2);

						startConnecting();
					
					}
					if(display.touchX() > 70 && display.touchX() < 220 && display.touchY() > 107 && display.touchY() < 135)
					{
						gameLevel = 3; // Set level to 3
						gameState = 2; // Starts game and chosen level
					
						// Send level
						byte byte1 = 0x7 << 4; // Send 7 code (send level)
						byte1 |= (serialNumber<<2); // 2 bits from serial number
					
						serialNumber++; // Update counter
						if(serialNumber == 4) serialNumber = 0;
					
						byte checkSum = gameLevel%4;
						byte1 |= (checkSum); // Add checksum to byte
						USART_putch(byte1);
					
						byte byte2 = gameLevel;
						USART_putch(byte2);

						startConnecting();
					
					}
					if(display.touchX() > 70 && display.touchX() < 220 && display.touchY() > 137 && display.touchY() < 165)
					{
						gameLevel = 4; // Set level to 4
						gameState = 2; // Starts game and chosen level
					
						// Send level
						byte byte1 = 0x7 << 4; // Send 7 code (send level)
						byte1 |= (serialNumber<<2); // 2 bits from serial number
					
						serialNumber++; // Update counter
						if(serialNumber == 4) serialNumber = 0;
					
						byte checkSum = gameLevel%4;
						byte1 |= (checkSum); // Add checksum to byte
						USART_putch(byte1);
					
						byte byte2 = gameLevel;
						USART_putch(byte2);
					
						startConnecting();
					}
					if(display.touchX() > 100 && display.touchX() < 210 && display.touchY() > 195 && display.touchY() < 230)
					{
						gameState = 1;
						DrawMainScreen();
					}
				
				}
			}
			else if (gameState == 9)
			{

				if(display.touchRead())
				{
					if(display.touchX() > 100 && display.touchX() < 210 && display.touchY() > 195 && display.touchY() < 230)
					{
						gameState = 1;
						DrawMainScreen();
					}

				}
			}
		}
	}

	return 0;
}

/************************************************************************/
/*	Function: ISR			                                            */
/*	-------------														*/
/*	Contains code executed every time timer 0 gives an interrupt. The	*/
/*	code handles when a tick must be executed. Because timer 0 cant		*/
/*	create 10hz, 100hz is being produced and a counter counts to 10		*/
/*	before a tick is executed.											*/
/*																		*/
/*	TIMER0_COMPA_vect: defines at which interrupt the code must be		*/
/*					   executed											*/
/*																		*/
/*	returns: -															*/
/************************************************************************/
ISR(TIMER0_COMPA_vect)
{
	// Interrupt code for TIMER 0 (100Hz)
	counter++;
	if ( counter >= 9 ) // Count to 10 and to tick
	{
		counter = 0;
		ticksleft--;
		dotick = 1;
	}
	
	TCNT0  = 0; // Reset timer
}

/************************************************************************/
/*	Function: ISR			                                            */
/*	-------------														*/
/*	Contains code executed every time timer 2 gives an interrupt. The	*/
/*	code handles the infrared communication. Timer 2 runs at 38 kHz		*/
/*																		*/
/*	TIMER0_COMPA_vect: defines at which interrupt the code must be		*/
/*					   executed											*/
/*																		*/
/*	returns: -															*/
/************************************************************************/
ISR(TIMER2_COMPA_vect)
{
	// Interrupt code for TIMER 2 (38kHz)
	/*if(byteToSend && irCounter <= 0 && !isReceiving) // If byte is sended
	{
		if(byteToSend & (1 << posCounter))
		{
			irCounter = 400;
		} 
		else 
		{
			irCounter = 200;
		}
		posCounter++;
		
		if(posCounter == 7)
		{
			posCounter = 0;
			byteToSend = 0;
		}
	}*/
	
	TCNT2 = 0; // Reset timer
}
