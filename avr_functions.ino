/************************************************************************/
/*	avr_functions.ino       KBS Microcontrollers						*/
/*																		*/
/*	All code regarding setting registers, like timers, in AVR			*/
/*																		*/
/************************************************************************/


/************************************************************************/
/*	Function: ADCsingleREAD                                             */
/*	-----------------------												*/
/*	Reads value from analog pin by setting and reading registers		*/
/*																		*/
/*	adctouse: 8 bit unsigned integer containing the analog pin number	*/
/*																		*/
/*	returns: value measured from given pin								*/
/************************************************************************/
int ADCsingleREAD(uint8_t adctouse)
{
	int ADCval;

	ADMUX = adctouse;         // use #1 ADC
	ADMUX |= (1 << REFS0);    // use AVcc as the reference
	ADMUX &= ~(1 << ADLAR);   // clear for 10 bit resolution
	
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);    // 128 prescale for 8Mhz
	ADCSRA |= (1 << ADEN);    // Enable the ADC

	ADCSRA |= (1 << ADSC);    // Start the ADC conversion

	while(ADCSRA & (1 << ADSC));      // This line waits for the ADC to finish

	ADCval = ADCL;
	ADCval = (ADCH << 8) + ADCval;    // ADCH is read so ADC can be updated again

	return ADCval;
}

/************************************************************************/
/*	Function: setupTimers	                                            */
/*	---------------------												*/
/*	Sets up all timers and interrupts									*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void setupTimers()
{
	cli(); // stop interrupts
	
	// TIMER 0 for interrupt frequency 100.16025641025641 Hz:
	TCCR0A = 0; // set entire TCCR0A register to 0
	TCCR0B = 0; // same for TCCR0B
	TCNT0  = 0; // initialize counter value to 0
	// set compare match register for 100.16025641025641 Hz increments
	OCR0A = 155; // = 16000000 / (1024 * 100.16025641025641) - 1 (must be <256)
	// turn on CTC mode
	TCCR0B |= (1 << WGM01);
	// Set CS02, CS01 and CS00 bits for 1024 prescaler
	TCCR0B |= (1 << CS02) | (1 << CS00);
	TCCR0B &= ~(1 << CS01);
	// Enable timer compare interrupt
	TIMSK0 |= (1 << OCIE0A);
	
	// TIMER 2 for interrupt frequency 38004.750593824225 Hz:
	TCCR2A = 0; // set entire TCCR2A register to 0
	TCCR2B = 0; // same for TCCR2B
	TCNT2  = 0; // initialize counter value to 0
	// set compare match register for 38004.750593824225 Hz increments
	OCR2A = 420; // = 16000000 / (1 * 38004.750593824225) - 1 (must be <256)
	// turn on CTC mode
	TCCR2B |= (1 << WGM21);
	// Set CS22, CS21 and CS20 bits for 1 prescaler
	TCCR2B |= (1 << CS20);
	TCCR2B &= ~(1 << CS22) & ~(1 << CS21);
	// enable timer compare interrupt
	TIMSK2 |= (1 << OCIE2A);
	
	sei(); // allow interrupts
}

/************************************************************************/
/*	Function: updateLives	                                            */
/*	---------------------												*/
/*	Sets level led indicators in right way so level is showed in pins	*/
/*	0, 1 and 2															*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void updateLives()
{
	switch (ownLives)
	{
		case 0:
			PORTC &= ~(1<<PINC0); // Set all 3 pins to 0
	        PORTC &= ~(1<<PINC1);
	        PORTC &= ~(1<<PINC2);
	        break;
	    case 1:
	        PORTC &= ~(1<<PINC0); // Set right 2 pins to 0
	        PORTC &= ~(1<<PINC1);
	        PORTC |= (1<<PINC2); // Set left pin to 1
	        break;
	    case 2:
	        PORTC &= ~(1<<PINC0); // Set right pin to 0
	        PORTC |= (1<<PINC1); // Set left 2 pins to 1
	        PORTC |= (1<<PINC2);
	        break;
	    case 3:
	        PORTC |= 0x7; // Set all 3 pins to 1
	        break;
	}
}
