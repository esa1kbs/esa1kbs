/************************************************************************/
/*	usart.ino       KBS Microcontrollers								*/
/*																		*/
/*	All code regarding serial communication with USART					*/
/*																		*/
/************************************************************************/


/************************************************************************/
/*	Function: USART_init												*/
/*  --------------------												*/
/*	Inits the serial connection with given baud rate using USART		*/
/*																		*/
/*	c:	character to send												*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void USART_init(unsigned long int baud)
{
    // Enable double speed
    UCSR0A |= (1 << U2X0);
    
    // Set baud rate
    int16_t ubbr = (F_CPU / (8ul * baud)) - 1;
    UBRR0H = (uint8_t) (ubbr / 256);
    UBRR0L = (uint8_t) (ubbr);

    // Default is async, no parity, 8 data bits, 1 stop bit
	UCSR0C = (1 << USBS0) | (1 << UCSZ00) | (1 << UPM01);   // 6 data bits, 1 stop bit
	UCSR0C |= (1 << UCSZ01);
	
    
    // Enable Receiver and Transmitter
    UCSR0B = (1 << TXEN0) | (1 << RXEN0);
}


/************************************************************************/
/*	Function: USART_putch												*/
/*  ---------------------												*/
/*	Writes one character to the transmit register of the USART			*/
/*	and waits until it is transmitted									*/
/*																		*/
/*	c:	character to send												*/
/*																		*/
/*	returns: sended character											*/
/************************************************************************/
int USART_putch(char c)
{
    // Wait for UDR to be empty
    while( !(UCSR0A & (1<<UDRE0)) )         // Loop while bit is set
	{
	}
    UDR0 = c;
    return (c);
}

/************************************************************************/
/*	Function: USART_getch												*/
/*  ---------------------												*/
/*	Gets one character from the receive register of the USART			*/
/*	It will wait until a character is available							*/
/*																		*/
/*	returns: received character											*/
/************************************************************************/
unsigned char USART_getch (void)
{
    return UDR0;
}

/************************************************************************/
/*	Function: USART_available											*/
/*  -------------------------											*/
/*	Checks if there is a message available								*/
/*																		*/
/*	returns: flag whether message is available							*/
/************************************************************************/
unsigned char USART_available(void)
{
	return UCSR0A & (1<<RXC0);
}