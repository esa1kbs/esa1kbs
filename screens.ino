/************************************************************************/
/*	Function: DrawMainScreen											*/
/*	------------------------											*/
/*	Draws the Main-screens with the buttons Play, Levels, Highscore		*/			
/*																		*/
/*	returns: void														*/
/************************************************************************/
void DrawMainScreen()
{
	// Background Main Screen black
	display.fillScreen(RGB(0,0,0));
	// Top Screen Title.
	display.drawText(15,7,"BOMBERMAN", RGB(255,255,255), RGB(0,0,0), 4);
	
	// Play Button
	display.drawText(97,60,"PLAY!",RGB(255,255,255), RGB(0,0,0), 3);
	// Level Button
	display.drawText(80,100,"LEVELS",RGB(255,255,255), RGB(0,0,0), 3);
	// HighScore Button
	display.drawText(43,140,"HIGHSCORE",RGB(255,255,255),RGB(0,0,0), 3);
}


/************************************************************************/
/*	Function: LevelScreen												*/
/*	---------------------												*/
/*	Draws the Level-screen with the buttons Level 1, Level 2, Level 3,	*/
/*	Level 4 and back(to mainscreen)										*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void LevelScreen()
{
	// Background Level Screen black
	display.fillScreen(RGB(0,0,0));
	// Top Screen Title.
	display.drawText(55,7,"Levels", RGB(255,255,255), RGB(0,0,0), 4);
	
	// Level 1 Button
	display.drawText(70,50,"Level 1",RGB(255,255,255), RGB(0,0,0), 3);
	// Level 2 Button
	display.drawText(70,80,"Level 2",RGB(255,255,255), RGB(0,0,0), 3);
	// Level 3 Button
	display.drawText(70,110,"Level 3",RGB(255,255,255),RGB(0,0,0), 3);
	// Level 4 Button
	display.drawText(70,140,"Level 4",RGB(255,255,255),RGB(0,0,0), 3);

	// Back to main-screen button
	display.drawRect(100,195,110,35,RGB(255,255,255));
	display.drawText(110,200,"Back", RGB(255,255,255), RGB(0,0,0), 3);
}

/************************************************************************/
/*	Function: HighscoreScreen											*/
/*	-------------------------											*/
/*	Draws the HighscoreScreen and reads the Highscores from the EEPROM	*/			
/*																		*/
/*	returns: void														*/
/************************************************************************/
void HighScoreScreen()
{
	// Background High Score Screen black
	display.fillScreen(RGB(0,0,0));
	// top Screen Title.
	display.drawText(15,7,"HighScore", RGB(255,255,255), RGB(0,0,0), 4);
	
	// Reads first highscore and name
	char p11 = EEPROM.read(0);
	char p12 = EEPROM.read(1);
	char p13 = EEPROM.read(2);
	int p1score = checkHighscore(0);

	// Reads second highscore and name
	char p21 = EEPROM.read(4);
	char p22 = EEPROM.read(5);
	char p23 = EEPROM.read(6);
	int p2score = checkHighscore(4);

	// Reads third highscore and name
	char p31 = EEPROM.read(8);
	char p32 = EEPROM.read(9);
	char p33 = EEPROM.read(10);
	int p3score = checkHighscore(8);

	// Highscores times 100
	p1score*=100;
	p2score*=100;
	p3score*=100;

	// Position 1.
	display.drawText(25,75,"1", RGB(211,24,24), RGB(0,0,0), 3);
	// Position 1 score.
	display.drawChar(50,75,p11,RGB(211,24,24), RGB(0,0,0), 3);
	display.drawChar(75,75,p12,RGB(211,24,24), RGB(0,0,0), 3);
	display.drawChar(100,75,p13,RGB(211,24,24), RGB(0,0,0), 3);
	display.drawInteger(190,75, p1score, 10, RGB(211,24,24), RGB(0,0,0), 3);
	
	// Position 2.
	display.drawText(25,115,"2", RGB(24,24,211), RGB(0,0,0), 3);
	// Position 2 score.
	display.drawChar(50,115,p21,RGB(24,24,211), RGB(0,0,0), 3);
	display.drawChar(75,115,p22,RGB(24,24,211), RGB(0,0,0), 3);
	display.drawChar(100,115,p23,RGB(24,24,211), RGB(0,0,0), 3);
	display.drawInteger(190,115,p2score, 10,  RGB(24,24,211), RGB(0,0,0), 3);

	// Position 3.
	display.drawText(25,155,"3", RGB(24,211,24), RGB(0,0,0), 3);
	// Position 3 score.
	display.drawChar(50,155,p31,RGB(24,211,24), RGB(0,0,0), 3);
	display.drawChar(75,155,p31,RGB(24,211,24), RGB(0,0,0), 3);
	display.drawChar(100,155,p31,RGB(24,211,24), RGB(0,0,0), 3);
	display.drawInteger(190,155,p3score, 10,  RGB(24,211,24), RGB(0,0,0), 3);

	// Back to main-screen button
	display.drawRect(100,195,110,35,RGB(255,255,255));
	display.drawText(110,200,"Back", RGB(255,255,255), RGB(0,0,0), 3);
}

/************************************************************************/
/*	Function: EndGameScreen												*/
/*	-----------------------												*/
/*	At the end of a game draws GAMEOVER on the Screen					*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void EndGameScreen()
{
	// Background End Game screen black
	display.fillScreen(RGB(0,0,0));
	// Draws rectangle middle of screen
	display.drawRect(10, 90, 300, 50, RGB(255,255,255));
	// Draws text middle of screen
	display.drawText(17, 102,"GAME OVER", RGB(255,255,255), RGB(0,0,0), 4);
}

/************************************************************************/
/*	Function: ScoreScreen												*/
/*	---------------------												*/
/*	Draws the score screen at the end of a game							*/
/*																		*/
/*	ScorePlayer1: is the score of the player zelf.						*/
/*  scorePlayer2: the score of the opponent								*/
/*	HighscoreBoolean: to check what kind of button needs to be placed	*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void ScoreScreen(int scorePlayer1, int scorePlayer2, char HighScoreBoolean)
{
	// Score times 100
	scorePlayer1 *= 100;
	scorePlayer2 *= 100;
	// Background Score Screen black
	display.fillScreen(RGB(0,0,0));
	// Top Screen Title.
	display.drawText(80,7,"Score", RGB(255,255,255), RGB(0,0,0), 4);
	// Draws the bigger rectangle middle of screen
	display.drawRect(10, 55, 300, 100, RGB(255,255,255));
	// Draws the smaller rectangle middle of screen
	display.drawRect(15, 60, 290, 90, RGB(255,255,255));
	
	// Player 1.
	display.drawText(25,75,"You:", RGB(211,24,24), RGB(0,0,0), 3);
	// Score Player 1.
	display.drawInteger(175,75, scorePlayer1, 10, RGB(211,24,24), RGB(0,0,0), 3);
	
	// Player 2.
	display.drawText(25,115,"Mate:", RGB(24,24,211), RGB(0,0,0), 3);
	// Score Player 2.
	display.drawInteger(175,115,scorePlayer2, 10,  RGB(24,24,211), RGB(0,0,0), 3);
	
	// If there is a highscore draws 'highscore!' else 'home' 
	if (HighScoreBoolean == 1 || HighScoreBoolean == 2 || HighScoreBoolean == 3 )
	{
		// Draw button next
		display.drawText(43,190,"HIGHSCORE!", RGB(255,255,255), RGB(0,0,0), 3);
	} 
	else
	{
		// Draw button home
		display.drawText(115,190,"Home", RGB(255,255,255), RGB(0,0,0), 3);
	}
}

/************************************************************************/
/*	Function: GetnameScreen												*/
/*	-----------------------												*/
/*	Draws the screen where the player that got an highscore can			*/
/*	enter his initials													*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void GetNameScreen()
{
	display.fillScreen(RGB(0,0,0));
	display.drawText(45,7,"Enter Name", RGB(255,255,255), RGB(0,0,0), 3);
	// First letter
	display.drawRect(45,36,60,34,RGB(255,255,255));
	display.fillTriangle(75,40,100,65,50,65,RGB(255,255,255));
	display.fillRect(45,74,60,52,RGB(255,255,255));
	display.drawRect(45,131,60,34,RGB(255,255,255));
	display.fillTriangle(75,160,100,135,50,135,RGB(255,255,255));
	
	// Second letter
	display.drawRect(125,36,60,34,RGB(255,255,255));
	display.fillTriangle(155,40,180,65,130,65,RGB(255,255,255));
	display.fillRect(125,74,60,52,RGB(255,255,255));
	display.drawRect(125,131,60,34,RGB(255,255,255));
	display.fillTriangle(155,160,180,135,130,135,RGB(255,255,255));

	// Third letter
	display.drawRect(205,36,60,34,RGB(255,255,255));
	display.fillTriangle(235,40,260,65,210,65,RGB(255,255,255));
	display.fillRect(205,74,60,52,RGB(255,255,255));
	display.drawRect(205,131,60,34,RGB(255,255,255));
	display.fillTriangle(235,160,260,135,210,135,RGB(255,255,255));

	// Save button
	display.drawRect(100,195,110,35,RGB(255,255,255));
	display.drawText(110,200,"Save", RGB(255,255,255), RGB(0,0,0), 3);
}
