/* 
	Editor: http://www.visualmicro.com
	        visual micro and the arduino ide ignore this code during compilation. this code is automatically maintained by visualmicro, manual changes to this file will be overwritten
	        the contents of the Visual Micro sketch sub folder can be deleted prior to publishing a project
	        all non-arduino files created by visual micro and all visual studio project or solution files can be freely deleted and are not required to compile a sketch (do not delete your own code!).
	        note: debugger breakpoints are stored in '.sln' or '.asln' files, knowledge of last uploaded breakpoints is stored in the upload.vmps.xml file. Both files are required to continue a previous debug session without needing to compile and upload again
	
	Hardware: Arduino/Genuino Uno, Platform=avr, Package=arduino
*/

#define __AVR_ATmega328p__
#define __AVR_ATmega328P__
#define _VMDEBUG 1
#define ARDUINO 10608
#define ARDUINO_MAIN
#define F_CPU 16000000L
#define __AVR__
#define F_CPU 16000000L
#define ARDUINO 10608
#define ARDUINO_AVR_UNO
#define ARDUINO_ARCH_AVR

int main(void);
int ADCsingleREAD(uint8_t adctouse);
void setupTimers();
void fillPosition(char field[][HEIGHT], int x, int y);
void drawObject(struct sobject *object);
void updateTime(int ticksleft, int framerate);
void moveObject(struct sobject *object, int dx, int dy, char field[][HEIGHT]);
char calculateMovement(struct sobject *player1, char field[][HEIGHT]);
void dropBomb(struct sobject *player, char field[][HEIGHT]);
void makeBombEffect(char field[][HEIGHT], int checkX, int checkY);
int updateBombEffect(char field[][HEIGHT], int x, int y, struct sobject *player1, struct sobject *player2);
void checkForPlayerDeath(struct sobject *player, int x, int y);
void checkForBombUpdates(struct sobject *player, struct sobject *otherplayer, char field[][HEIGHT]);
void checkForBombeffectsUpdates(struct sobject *player, struct sobject *otherplayer, char field[][HEIGHT]);
void readData();
void readData();
void DrawMainScreen();
void drawLayout();
void EndGameScreen();

#include "pins_arduino.h" 
#include "arduino.h"
#include "ESA1KBS.ino"
#include "avr_functions.ino"
#include "draw_functions.ino"
#include "game_logic.ino"
#include "mainscreen.ino"
