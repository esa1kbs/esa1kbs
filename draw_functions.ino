/************************************************************************/
/*	draw_functions.ino       KBS Microcontrollers						*/
/*																		*/
/*	All code regarding drawing things on the screen, including drawing	*/
/*	blocks, players, etc.												*/
/*																		*/
/************************************************************************/


/************************************************************************/
/*	Function: initiateScreen											*/
/*	------------------------											*/
/*	Draws the screen by reading all positions in field array			*/
/*																		*/
/*	field: 2-dimensional array containing all the positions in the		*/
/*		   field. Positions are stored as char.							*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void initiateScreen(char field[][HEIGHT])
{
	int x, y;
	// Draw the grid (for every x and y)
	for(x=0; x<countWidth; x++)
	{
		for(y=0; y<countHeight; y++)
		{
			// Draw square for every block to create grid
			display.drawRect(x*blockWidth, topy + y*blockWidth, blockWidth, blockWidth, RGB(0,0,0));
			
			fillPosition(field, x, y); // Fill all blocks correspondingly
		}
		
	}
	
	return;
}

/************************************************************************/
/*	Function: fillPosition												*/
/*	------------------------											*/
/*	Draws given position in the field in correct way					*/
/*																		*/
/*	field: 2-dimensional array containing all the positions in the		*/
/*		   field. Positions are stored as char.							*/
/*	x:	   the x-position of the position								*/
/*	y:	   the y-position of the position								*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void fillPosition(char field[][HEIGHT], int x, int y)
{
	display.drawRect(x*blockWidth, topy + y*blockWidth, blockWidth, blockWidth, RGB(0,0,0)); // Make first black
	
	if(field[x][y] == 0) // Empty position
	{
		display.fillRect(x*blockWidth, topy + y*blockWidth, blockWidth, blockWidth, RGB(0,0,0));
	}
	else if(field[x][y] == 1) // Obstacle or chest
	{
		int startx=x*blockWidth+1;
		int starty=topy+y*blockWidth+1;
		display.fillRect(startx, starty, blockWidth-2, blockWidth-2, RGB(128,49,8));
		display.drawRect(startx, starty, blockWidth-2, blockWidth-2, RGB(52,21,5));
		display.drawLine(startx,starty+33*blockWidth/100+1,startx+66*blockWidth/100-3,starty+blockWidth-3,RGB(52,21,5));
		display.drawLine(startx+33*blockWidth/100+1,starty,startx+blockWidth-3,starty+66*blockWidth/100-3,RGB(52,21,5));
		display.drawLine(startx+66*blockWidth/100,starty+33*blockWidth/100,startx+blockWidth-3,starty+33*blockWidth/100,RGB(52,21,5));
		display.drawLine(startx,starty+66*blockWidth/100,startx+33*blockWidth/100,starty+66*blockWidth/100,RGB(52,21,5));
	}
	else if(field[x][y] == 2) // Solid block or wall
	{
		display.fillRect(x*blockWidth, topy+y*blockWidth, blockWidth, blockWidth, RGB(222,102,4));
		for(int i=0; i<4; i++)
		{
			int startx=x*blockWidth;
			int starty=topy+y*blockWidth;
			display.drawLine(startx, starty+i*blockWidth/4, startx+blockWidth, starty+i*blockWidth/4,RGB(0,0,0));
			display.drawLine(startx+(i%2+1)*blockWidth/3,starty+i*blockWidth/4,startx+(i%2+1)*blockWidth/3,starty+i*blockWidth/4+blockWidth/4,RGB(0,0,0));
		}
		
	}
	else if(field[x][y] == 3) // Bomb
	{
		display.fillRect(x*blockWidth+2, topy + y*blockWidth+2, blockWidth-4, blockWidth-4, RGB(0,0,0));
		display.drawCircle(x*blockWidth+poppetje[8], topy+y*blockWidth+poppetje[8], poppetje[6], RGB(255,255,255));
		display.fillCircle(x*blockWidth+poppetje[8], topy+y*blockWidth+poppetje[8], poppetje[5], RGB(100,0,135));
		display.drawLine(x*blockWidth+poppetje[8], topy+y*blockWidth+poppetje[1],x*blockWidth+poppetje[8],topy+y*blockWidth+poppetje[4],RGB(102,102,102));
		display.drawPixel(x*blockWidth+poppetje[8],topy+y*blockWidth+poppetje[1],RGB(255,255,0));
		
	}
	else if(field[x][y] == 4) // Bomb effect
	{
		display.fillRect(x*blockWidth+2, topy + y*blockWidth+2, blockWidth-4, blockWidth-4, RGB(0,0,0));
		display.fillRoundRect(x*blockWidth,topy+y*blockWidth,blockWidth,blockWidth,10, RGB(255,0,0));
		display.fillRoundRect(x*blockWidth+5,topy+y*blockWidth+5, blockWidth-10,blockWidth-10, 5, RGB(255,128,0));
		display.fillCircle(x*blockWidth+poppetje[8],topy+y*blockWidth+poppetje[8],3,RGB(255,255,0));
	}
	
	return;
}

/************************************************************************/
/*	Function: drawObject												*/
/*	--------------------												*/
/*	Draws object, like a player, on the screen. Object id 1 is red		*/
/*	player and object id 2 is blue player								*/
/*																		*/
/*	object:		   struct with all data from object, like x, y, etc.	*/
/*	makeInvisible: flag for making object invisible. This is 0 or 1. It	*/
/*				   is used for flashing the player on the screen at		*/
/*				   start of the game.									*/
/*	field:		   2-dimensional array containing all the positions in	*/
/*				   the	field. Positions are stored as char.			*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void drawObject(struct object *object, char makeInvisible, char field[][HEIGHT])
{
	int bwX = object->x*blockWidth; // Upper left x-position
	int bwY = topy+object->y*blockWidth; // Upper left y-position
	switch(object->id)
	{
		case 1: // Player 1
			display.fillRect(bwX+poppetje[2],bwY+poppetje[10],poppetje[3], poppetje[3],RGB(255,255,255));
			display.fillRect(bwX+poppetje[11],bwY+poppetje[10],poppetje[3], poppetje[3],RGB(255,255,255));
			display.fillCircle(bwX+poppetje[5],bwY+poppetje[12],poppetje[1],RGB(255,255,255));
			display.fillCircle(bwX+poppetje[9],bwY+poppetje[12],poppetje[1],RGB(255,255,255));
			display.fillEllipse(bwX+poppetje[8],bwY+poppetje[9], poppetje[8]-3,poppetje[4],RGB(211,24,24));
			display.fillEllipse(bwX+poppetje[8],bwY+poppetje[5], poppetje[8]-3,poppetje[4],RGB(255,204,144));
			display.fillCircle(bwX+poppetje[5],bwY+poppetje[7],poppetje[0],RGB(139,69,19));
			display.fillCircle(bwX+poppetje[9],bwY+poppetje[7],poppetje[0],RGB(139,69,19));
			break;
			
		case 2: // Player 2
			display.fillRect(bwX+poppetje[2],bwY+poppetje[10],poppetje[3], poppetje[3],RGB(255,255,255));
			display.fillRect(bwX+poppetje[11],bwY+poppetje[10],poppetje[3], poppetje[3],RGB(255,255,255));
			display.fillCircle(bwX+poppetje[5],bwY+poppetje[12],poppetje[1],RGB(255,255,255));
			display.fillCircle(bwX+poppetje[9],bwY+poppetje[12],poppetje[1],RGB(255,255,255));
			display.fillEllipse(bwX+poppetje[8],bwY+poppetje[9], poppetje[8]-3,poppetje[4],RGB(24,24,211));
			display.fillEllipse(bwX+poppetje[8],bwY+poppetje[5], poppetje[8]-3,poppetje[4],RGB(255,204,144));
			display.fillCircle(bwX+poppetje[5],bwY+poppetje[7],poppetje[0],RGB(139,69,19));
			display.fillCircle(bwX+poppetje[9],bwY+poppetje[7],poppetje[0],RGB(139,69,19));
			break;
			
	}
	
	return;
}

/************************************************************************/
/*	Function: updateTime												*/
/*	------------------------											*/
/*	Draws the time until end of game on the screen in correct format,	*/
/*	like 1:43. Seconds left in a minute is calculated by using			*/
/*	total numbers of seconds modulus 60.								*/
/*																		*/
/*	ticksleft: current game time, measured in ticks that are left to	*/
/*			   end of game.												*/
/*	framerate: frame rate of the game, number of ticks executed per		*/
/*			   second.													*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void updateTime(int ticksleft, int framerate)
{
	int seconds = ticksleft/framerate; // Seconds left
	if(ticksleft%framerate == 0) // Execute every second
	{
		int secLeft = seconds%60; // Calculate seconds left per minute
		int minutes = (seconds - secLeft)/60; // Calculate minutes left
		// Show time
		display.drawInteger(125, 10, (int)minutes, 10, RGB(255,255,255), RGB(0,0,0), 2);
		display.drawChar(137, 10, ':', RGB(255,255,255), RGB(0,0,0), 2);
		display.fillRect(158, 10, 20, 20, RGB(0,0,0));
		display.drawInteger(150, 10, (int)secLeft, 10, RGB(255,255,255), RGB(0,0,0), 2);
	}
	
	return;
}
