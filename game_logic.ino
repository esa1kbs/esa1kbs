/************************************************************************/
/*	game_logic.ino       KBS Microcontrollers							*/
/*																		*/
/*	Contians all code regarding the game logic. This includes field		*/
/*	generation, moving objects, dropping bombs, player level handling	*/
/*	and communication.													*/
/*																		*/
/************************************************************************/

#define WIDTH 11
#define HEIGHT 7

/************************************************************************/
/*	Function: generateField												*/
/*																		*/
/*	For every position random generateNumber:							*/
/*	0: empty															*/
/*	1: obstacle															*/
/*	For all even coordinates save solid block in grid					*/
/*	2: solid block														*/
/*	Makes starting position in the corner (3 blocks) empty and places a	*/
/*	box next to it														*/
/*																		*/
/*	field:	   2-dimensional array containing all the positions in the	*/
/*			   field. Positions are stored as a char.					*/
/*	randomInt: seed used for generating the playing field				*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void generateField(char field[][HEIGHT], byte randomInt)
{
	int x, y; // Holds coordinates
	int counteven = 0, countodd = 0;
	for(x=0; x<countWidth; x++) // For every X in grid
	{
		for(y=0; y<countHeight; y++) // For every Y in grid
		{
			
			int generatedNumber = (int)((float)randomInt/(float)(x*randomInt%47+15.357)/15.745*(float)(y*randomInt%74+15.654)/15.954*(float)(randomInt%9/3+3.753)*randomInt*15/13.64);
			
			
			if((generatedNumber)%2 == 0) { // In 50% of cases save obstacle in grid
				field[x][y] = 1;
			} else {
				field[x][y] = 0;
			}
			

			if(gameLevel == 1){
				if((x%2 && y%2)) { // For all even coordinates save solid block in grid (creates pillar pattern)
					field[x][y] = 2;
				}
			} else if(gameLevel == 2){
				if(((x==1 || x==9) && (y>0 && y<6) || (y==1 || y==5) && (x>0 && x<10)) && x!=3 && x!=5 && x!=7 && y!=3) {
					field[x][y] = 2;
				}
				field[3][3] = 2;
				field[5][3] = 2;
				field[7][3] = 2;
				
			} else if(gameLevel == 3){
				if(y>=0 && y<=6){
					field[x][y] = 1;
				}
			} else if(gameLevel == 4){
				if(((y==1 || y==5) && (x>0 && x<10) && x!=5) || ((x==2 && x==3 && x==7 && x==8)&&( y>0 && y<6) && y!=3)){
					field[x][y] = 2;
					
				}
				field[0][3] = 2;
				field[5][3] = 2;
				field[10][3] = 2;
				field[2][2] = 2;
				field[3][2] = 2;
				field[7][2] = 2;
				field[8][2] = 2;
				field[2][4] = 2;
				field[3][4] = 2;
				field[7][4] = 2;
				field[8][4] = 2;
			}
		}
	}
	
	// Make top left and bottom right positions empty and add boxes as starting barrier
	field[0][0] = 0;
	field[0][1] = 0;
	field[0][2] = 1;
	field[1][0] = 0;
	field[2][0] = 1;
	field[WIDTH-1][HEIGHT-1] = 0;
	field[WIDTH-1][HEIGHT-2] = 0;
	field[WIDTH-1][HEIGHT-3] = 1;
	field[WIDTH-2][HEIGHT-1] = 0;
	field[WIDTH-3][HEIGHT-1] = 1;
	
	return;
	
}

/************************************************************************/
/*	Function: moveObject												*/
/*	--------------------												*/
/*	Displays player at current position and draws previous position		*/
/*	black again. Also checks if player can move in a specific direction	*/
/*	and if player moves through an explosion. If a player moves through	*/
/*	an explosion, lives are updated.									*/
/*																		*/
/*	object: struct with all data from obect, like x, y , etc.			*/
/*	dx:		difference in x, for moving the player left or right		*/	
/*			-1: left													*/
/*			+1: right													*/
/*	dy:		difference in y, for moving the player up or down			*/
/*			-1: up														*/	
/*			+1: down													*/		
/*	field:	2-dimensional array containing all the positions in the		*/
/*			field. Positions are stored as a char.						*/
/*																		*/		
/*	returns: void														*/
/************************************************************************/
void moveObject(struct object *object, int dx, int dy, char field[][HEIGHT])
{
	// Check possibility of movement with edge of field
	if(!( (object->x + dx) < 0 || (object->x + dx) >= countWidth || (object->y + dy) < 0 || (object->y + dy) >= countHeight ))
	{
		// Check possibility of movement with blocks
		int playerX = object->x;
		int playerY = object->y;
		if(field[playerX+dx][playerY+dy] != 1 && field[playerX+dx][playerY+dy] != 2 && field[playerX+dx][playerY+dy] != 3)
		{
			fillPosition(field, object->x, object->y);
			object->x += dx;
			object->y += dy;
			drawObject(player2, 0, field); // Draw other player
			drawObject(object, 0, field); // Draw own player (draw last so own player is on top)
			
			// If own player send data to other player
			if(object == player1)
			{
				byte byte1 = 0x2 << 4;
				byte byte2 = (object->x<<4)|object->y; // Set coordinates in byte 2
				byte1 |= serialNumber<<2;
				serialNumber++; // Increase serial number and check if it can be reset
				if(serialNumber == 4) serialNumber = 0;
				byte1 |= byte2%4; // Checksum = data modulus 4
				USART_putch(byte1);
				USART_putch(byte2);
			}
		}
		if(field[playerX+dx][playerY+dy] == 4 && object->id == Iam)
		{
			ownLives--;
		}
	}
	return; 
}

/************************************************************************/
/*	Function: moveObjectAbsolute										*/
/*  ----------------------------										*/
/*	For moving a player to a absolute position							*/
/*                                                                      */
/*	object: struct with all data from obect, like x, y , etc.			*/
/*	x:		x-position to move to										*/
/*	y:		y-position to move to										*/
/*	field:	2-dimensional array containing all the positions in the		*/
/*			field. Positions are stored as a char.						*/
/*																		*/
/*	returns: void														*/
/************************************************************************/
void moveObjectAbsolute(struct object *object, int x, int y, char field[][HEIGHT])
{
	// Check possibility of movement with edge of field
	if(!( (x) < 0 || (x) >= countWidth || (y) < 0 || (y) >= countHeight ))
	{
		// Check possibility of movement with blocks
		int playerX = object->x;
		int playerY = object->y;
		if(field[x][y] != 1 && field[x][y] != 2 && field[x][y] != 3)
		{
			fillPosition(field, object->x, object->y);
			object->x = x;
			object->y = y;
			drawObject(object, 0, field);
			
			// If own player send data to other player
			if(object->id == Iam)
			{
				byte byte1 = 0x2 << 4;
				byte byte2 = (object->x<<4)|object->y; // Set coordinates in byte 2
				byte1 |= serialNumber<<2;
				serialNumber++; // Increase serial number and check if it can be reset
				if(serialNumber == 4) serialNumber = 0;
				byte1 |= byte2%4; // Checksum = data modulus 4
				USART_putch(byte1);
				USART_putch(byte2);
			}
		}
		
		if(field[x][y] == 4 && object->id == Iam) // There is an explosion effect
		{
			ownLives--;
		}
	}
	
	return;
}

/************************************************************************/
/*	Function: calculateMovement											*/
/*  ---------------------------											*/
/*	Calculates the direction in which the player is moving by reading	*/ 
/*	the nunchuck data.													*/
/*                                                                      */
/*	player1: struct with all data from player, like x, y , etc.			*/
/*	field:	 2-dimensional array containing all the positions in the	*/
/*			 field. Positions are stored as a char.						*/
/*																		*/
/*	returns: winddirection												*/
/************************************************************************/
char calculateMovement(struct object *player1, char field[][HEIGHT])
{
	char winddirection;
	
	waitToMove--; // For slowing down character speed
	if(waitToMove <= 0) // Only move every couple of ticks, so player moves not too fast
	{
		// Calculate angle
		int direction = atan2(nunchuk.analogX-131,nunchuk.analogY-127)*(180/3.14159265); // Calculate direction of nunchuk in degrees
		if(direction < 0) // Make negative degrees positive
		{
			direction = direction + 360;
		}
		
		// Interpret direction of movement
		if(sqrt(abs(131-nunchuk.analogX)+abs(127-nunchuk.analogY) > 70))
		{
			if(direction < 45 || direction >= 315) {
				winddirection = 'N';
				} else if(direction >= 45 && direction < 135) {
				winddirection = 'O';
				} else if(direction >= 135 && direction < 225) {
				winddirection = 'Z';
				} else {
				winddirection = 'W';
			}
			} else {
			winddirection = ' ';
		}
		
		waitToMove = ownRunningSpeed; // Set speed
		
	}
	
	return winddirection;
}

/************************************************************************/
/*	Function: dropBomb													*/
/*  ------------------													*/
/*	Places the bomb at the given position, counts down when the bomb is */
/*	placed, checks how many bombs are placed by a certain player.		*/
/*                                                                      */
/*	player:	   struct with all data from player, like x, y , etc.		*/
/*	positionX: contains the x value of the position at which the bomb	*/
/*			   is placed.												*/
/*	positionY: contains the y value of the position at which the bomb	*/
/*			   is placed.												*/
/*	field:	   2-dimensional array containing all the positions in the	*/
/*			   field. Positions are stored as a char.					*/
/*																		*/
/* returns: void														*/
/************************************************************************/
void dropBomb(struct object *player, char positionX, char positionY, char field[][HEIGHT])
{
	if(player->id == Iam)
	{
		if(ownBombLimit <= 0) return;
	} 
	else if(player->id == otherIs)
	{
		if(otherBombLimit <= 0) return;
	}
	
	// Make bomb object
	if(field[positionX][positionY] != 3) // If there is no bomb already
	{
		// Make and fill struct with bomb info
		struct object bomb;
		bomb.x = positionX;
		bomb.y = positionY;
		bomb.age = 20; // Ticks
		bomb.owner = player->id; // Make player owner
		bomb.id = bombIdCounter;
		// Add to array
		if(bombIdCounter >= bombsArraySize)
		{
			bombIdCounter = 0;
		}
		bombs[bombIdCounter] = bomb;
		bombIdCounter++;
		if(player->id == Iam)
		{
			ownBombLimit--;
			
			// Send to other player
			byte byte1 = 0x4 << 4;
			byte byte2 = (positionX<<4)|positionY; // Set coordinates in byte 2
			byte1 |= serialNumber<<2;
			serialNumber++; // Increase serial number and check if it can be reset
			if(serialNumber == 4) serialNumber = 0;
			byte1 |= byte2%4; // Checksum = data modulus 4
			USART_putch(byte1);
			USART_putch(byte2);
		}
		else if(player->id == otherIs)
		{
			otherBombLimit--;
		}
		
		// Update field
		field[positionX][positionY] = 3;
		fillPosition(field, positionX, positionY);
		// Redraw player
		drawObject(player, 0, field);
		
	}
	return;
}

/************************************************************************/
/*	Function: makeBombEffect											*/
/*  ------------------------											*/
/*	Places bombeffect on given position									*/
/*                                                                      */
/*	field:  2-dimensional array containing all the positions in the		*/
/*			field. Positions are stored as a char.						*/
/*	checkX: contains x of given position								*/
/*	checkY: contains y of given position								*/
/*                                                                      */
/*	returns: void														*/
/************************************************************************/
void makeBombEffect(char field[][HEIGHT], int checkX, int checkY)
{
	fillPosition(field, checkX, checkY);
	struct object bombeffect;
	bombeffect.x = checkX;
	bombeffect.y = checkY;
	bombeffect.age = 15; // Ticks
	bombeffect.id = bombeffectIdCounter;
	// Add to array
	if(bombeffectIdCounter >= bombeffectsArraySize) // If counter is larger than array size, set counter to zero to start again in array
	{
		bombeffectIdCounter = 0;
	}
	bombeffects[bombeffectIdCounter] = bombeffect; // Add bomb to array
	bombeffectIdCounter++; // Update counter
	return;
}

/************************************************************************/
/*	Function: updateBombEffect											*/
/*  --------------------------											*/
/*	Checks if explosion hits objects and must go on and updates objects	*/
/*                                                                      */
/*	field:	 2-dimensional array containing all the positions in the	*/
/*			 field. Positions are stored as a char.						*/
/*	x:		 contains x of given position								*/
/*	y:		 contains y of given position								*/
/*	player1: struct with all data from player, like x, y , etc.			*/
/*	player2: struct with all data from player, like x, y , etc.			*/
/*                                                                      */
/*	returns: 1 or 0														*/
/************************************************************************/
int updateBombEffect(char field[][HEIGHT], int x, int y, struct object *player1, struct object *player2)
{
	if(field[checkX][checkY] == 0) {
		// Make explosion and continue
		field[checkX][checkY] = 4;
		// Check if player is on position
		checkForPlayerDeath(player1, checkX, checkY);
		//checkForPlayerDeath(player2, checkX, checkY);
	} else if(field[checkX][checkY] == 1) {
		// Make explosion and stop
		field[checkX][checkY] = 4;
		return 0;
	} else if(field[checkX][checkY] == 2) {
		// No explosion
		if(x < 0) {
			checkX--;
			} else if(x > 0) {
			checkX++;
			} else if(y < 0) {
			checkY--;
			} else if(y > 0) {
			checkY++;
		}
		return 0;
	}
	return 1;
}

/************************************************************************/
/*	Function: checkForPlayerDeath										*/
/*  -----------------------------										*/
/*	Checks if player position is equal to the position of explosion  	*/
/*	If so update lifes (-1)												*/
/*                                                                      */
/*	x:		contains x of the bomb explosion							*/
/*	y:		contains y of the bomb explosion							*/
/*	player: struct with all data from player, like x, y , etc.			*/
/*                                                                      */
/*	returns: void														*/
/************************************************************************/
void checkForPlayerDeath(struct object *player, int x, int y)
{
	if(player->x == x && player->y == y) // If positions are equal
	{		
		ownLives--;	
	}
	return;
}

/************************************************************************/
/*	Function: checkForBombUpdates										*/
/*  -----------------------------										*/
/*	Checks bomb "age" for exploding the bomb, makes explosion, checks	*/
/*	if player is killed and checks explosion in each direction			*/
/*                                                                      */
/*	field:		 2-dimensional array containing all the positions in	*/
/*				 the field. Positions are stored as a char.				*/
/*	player:		 struct with all data from player, like x, y , etc.		*/
/*	otherplayer: struct with all data from otherplayer, like x, y, etc.	*/
/*                                                                      */
/*	returns: void														*/
/************************************************************************/
void checkForBombUpdates(struct object *player, struct object *otherplayer, char field[][HEIGHT])
{
	// Check for bomb updates
	for(int i = 0; i < bombsArraySize; i++)
	{
		if(bombs[i].age > 0) // If bomb can age
		{
			bombs[i].age--; // Age bomb
		}
		if(bombs[i].age == 1) // If age is equal to 1, let bomb explode
		{
			// Explosion!
			if(bombs[i].owner == Iam) // If player owns bomb
			{
				ownBombLimit++; // Rise own bomb limit
			} 
			else if(bombs[i].owner == otherIs)
			{
				otherBombLimit++; // Rise other bomb limit
			}

			int bombX = bombs[i].x;
			int bombY = bombs[i].y;
			// Set checking positions on starting point
			checkX = bombX;
			checkY = bombY;
			int continueChecking = 1; // Stays 1 when positions have to be checked
			
			// Make explosion on spot of bomb
			field[bombX][bombY] = 4;
			checkForPlayerDeath(player, bombX, bombY); // Check if player is killed
			makeBombEffect(field, bombX, bombY); // Draw a bomb effect
			
			bombs[i].age--;
			
			// Now for other positions in sight (for every direction)
			while(continueChecking)
			{
				// WEST
				checkX--;
				if(bombX - checkX < ownBombDistance && checkX >= 0 && checkX < countWidth)
				{
					continueChecking = updateBombEffect(field, 1, 0, player, otherplayer);
					makeBombEffect(field, checkX, checkY);
					} else {
					continueChecking = 0;
				}
				
			}
			
			checkX = bombX;
			continueChecking = 1;
			
			while(continueChecking)
			{
				// EAST
				checkX++;
				if(checkX - bombX < ownBombDistance && checkX >= 0 && checkX < countWidth)
				{
					continueChecking = updateBombEffect(field, -1, 0, player, otherplayer);
					makeBombEffect(field, checkX, checkY);
					} else {
					continueChecking = 0;
				}
				
			}
			
			checkX = bombX;
			checkY = bombY;
			continueChecking = 1;
			
			while(continueChecking)
			{
				// NORTH
				checkY--;
				if(bombY - checkY < ownBombDistance && checkY >= 0 && checkY < countHeight)
				{
					continueChecking = updateBombEffect(field, 0, 1, player, otherplayer);
					makeBombEffect(field, checkX, checkY);
					} else {
					continueChecking = 0;
				}
				
			}
			
			checkY = bombY;
			continueChecking = 1;
			
			while(continueChecking)
			{
				// SOUTH
				checkY++;
				if(checkY - bombY < ownBombDistance && checkY >= 0 && checkY < countHeight)
				{
					continueChecking = updateBombEffect(field, 0, -1, player, otherplayer);
					makeBombEffect(field, checkX, checkY);
					} else {
					continueChecking = 0;
				}
				
			}
			
		}
	}
	return;
}

/************************************************************************/
/*	Function: checkForBombeffectsUpdates								*/
/*  ------------------------------------								*/
/*	Draws and redraws position of explosion, bomb and player			*/
/*                                                                      */
/*	player: struct with all data from player, like x, y , etc.			*/
/*			otherplayer: struct with all data from otherplayer, like x, */
/*			y, etc.														*/
/*	field:  2-dimensional array containing all the positions in the		*/
/*			field. Positions are stored as a char.						*/
/*                                                                      */
/*	returns: void														*/
/************************************************************************/
void checkForBombeffectsUpdates(struct object *player, struct object *otherplayer, char field[][HEIGHT])
{
	// Check for bomb effects updates
	for(int i = 0; i < bombeffectsArraySize; i++)
	{
		if(bombeffects[i].age > 0) // If bomb effect is still alive
		{
			bombeffects[i].age--; // Age bomb effect
		}
		if(bombeffects[i].age == 1) // When age is 1, remove bomb effect from screen
		{
			// Delete bomb from field
			field[bombeffects[i].x][bombeffects[i].y] = 0; // Set position to empty
			fillPosition(field, bombeffects[i].x, bombeffects[i].y); // Draw position
			drawObject(player, 0, field); // Redraw player in case player is on position bomb
			drawObject(otherplayer, 0, field); // Redraw player in case player is on position bomb
			
			bombeffects[i].age--;
		}
	}
	return;
}

/************************************************************************/
/*	Function: readData													*/
/*  ------------------													*/
/*	Contains the communication code between the 2 arduino's for reading	*/
/*	messages.															*/
/*																		*/
/*	player2: struct with all data from player2, like x, y , etc. It is	*/
/*			 used for moving the player on screen.						*/
/*	field:   2-dimensional array containing all the positions in the	*/
/*			 field. Positions are stored as a char.						*/
/*                                                                      */
/*	returns: void														*/
/************************************************************************/

void readData(struct object *player2, char field[][HEIGHT])
{
	if(USART_available())
	{
		
		byte input = USART_getch();
		byte interpret = input | 0x0f;
		byte inputSN = input & 0b00001100 >> 2;
		byte checksum = input & 0b00000011;
		serialNumber++;
		if(serialNumber==4)serialNumber = 0;
		
		lastNumber = inputSN;
		if(inputSN!=serialNumber){
			errorCounter++;
		}
		
		switch(interpret)
		{
			case 0xff://AUA
				if(gameState == 2) // If connecting, wait for AUA
				{
					// AUA received, I am slave
					// Send synced
					byte byte1 = 0xA << 4; // Send A code (synced)
					byte1 |= (serialNumber<<2); // 2 bits from serial number
					USART_putch(byte1);
					serialNumber++;
					if(serialNumber == 4) serialNumber = 0;
					
				}
				break;
			case 0xef://building field
				if(USART_available()) // I am slave, make field from seed
				{
					byte seed = USART_getch(); // Read seed
					if(checksum == seed%4){ // If checksum is right
						fieldSeed = seed; // Set sent seed in memory
						connected = 1; // Set connected flag to true, so field will be drawn and game started

						Iam = 2;
						otherIs = 1;
					}
				}
				break;
			case 0xdf://field built
				
				break;
			case 0xcf://score
				if(USART_available())
				{
					byte score = USART_getch(); // Read score data
					if(checksum == score%4)
					{
						EndScorePlayer2 = score;
					}
				}
				break;
			case 0xbf://Error resend
				
				break;
			case 0xaf://synced
				if(gameState == 2) // Connecting, so I am master
				{
					// Generate random seed
					fieldSeed = random(100, 255);
					
					// Send seed
					byte byte1 = 0xE << 4; // Send E code (start sending positions)
					byte1 |= (serialNumber<<2); // 2 bits from serial number
					
					serialNumber++; // Update counter
					if(serialNumber == 4) serialNumber = 0;
					
					byte checkSum = fieldSeed%4;
					byte1 |= (checkSum); // Add checksum to byte
					USART_putch(byte1);
					
					byte byte2 = fieldSeed;
					USART_putch(byte2);
					
					// Generate own field
					connected = 1;

					Iam = 1;
					otherIs = 2;

				}
				break;
			case 0x9f://DEATH
				// Continue to next game state
				TimeLeft = ticksleft;
				
				// Send score
				sendScore();
				
				gameState = 5;
				EndGameScreen();
				ticksleft = 3*framerate; // 3 sec countdown before next gameState.
				break;
			case 0x8f://desync
				
				break;
			case 0x7f://level
				if(USART_available()) // I am slave, make field from seed
				{
					byte level = USART_getch(); // Read level data
					if(checksum == level%4)
					{
						gameLevel = level;
					}
				}
				break;
			case 0x4f://bomb
				byte positions, placex, placey;
				if(USART_available())
				{
					positions=USART_getch();
					if(checksum == positions%4)
					{
						placex = (positions & 0b11110000) >> 4;
						placey = positions & 0b00001111;
						dropBomb(player2, placex, placey, field);
					}
				}
				break;
			case 0x2f://player
				byte movetotal;
				byte movex;
				byte movey;
				if(USART_available())
				{
					movetotal= USART_getch();
					if(checksum == movetotal%4)
					{
						movey = movetotal & 0b00001111;
						movex = (movetotal & 0b11110000) >> 4;
						moveObjectAbsolute(player2, movex, movey, field);
					}
				}
				break;
			case '0x2f'://timer
				
				break;
		}
	}
	return;
}

/************************************************************************/
/*	Function: startConnecting											*/
/*  -------------------------											*/
/*	Makes screen ready for connecting state								*/
/*                                                                      */
/*	returns: void														*/
/************************************************************************/
void startConnecting()
{
	display.fillScreen(RGB(0,0,0));
	display.drawText(50, 100, "Connecting...", RGB(255,255,255), RGB(0,0,0), 2);
	ticksleft = 60 * framerate;
	return;
}

/************************************************************************/
/*	Function: sendScore													*/
/*  -------------------													*/
/*	Calculates score and send score to other player						*/
/*                                                                      */
/*	returns: void														*/
/************************************************************************/
void sendScore()
{
	// Calculate score of player
	EndScorePlayer1 = CalculateScore(TimeLeft, ownLives);
	
	// Send score
	for(int i=0; i<20; i++)
	{
		byte scorebyte1 = 0xC << 4; // Send C code (send score)
		scorebyte1 |= (serialNumber<<2); // 2 bits from serial number
	
		serialNumber++; // Update counter
		if(serialNumber == 4) serialNumber = 0;
	
		byte checkSum = EndScorePlayer1%4;
		scorebyte1 |= (checkSum); // Add checksum to byte
		USART_putch(scorebyte1);
	
		byte scorebyte2 = EndScorePlayer1;
		USART_putch(scorebyte2);
	}
	
	return;
}

/************************************************************************/
/*	Function: startVar			                                        */
/*	------------------													*/
/*	Resets all the data needed to start a new game						*/
/*	Resets all the data needed to start a new game						*/
/*																		*/
/*	returns: -															*/
/************************************************************************/
void startVar()
{
	// reset all data needed to create a new game.
	bombIdCounter = 0; // Holds current position in array of bombs
	bombeffectIdCounter = 0; // Holds current position in array of bomb effects
	ownBombLimit = 2; // Bomb limit
	otherBombLimit = 2; // Bomb limit other player

	ownLives = 3; // Counts lives left
	otherLives = 3; // Counts lives other player left

	ownBombDistance = 3; // Bomb distance
	otherBombDistance = 3; // Bomb distance other player

	ownRunningSpeed = 1; // Speed running (1 is fast, higher is slower ;))
	otherRunningSpeed = 1; // Speed other player

	connected = 0;
	//gameLevel = 1; // Reset level to standard

	// Make player 1 and 2
	player1->id = Iam;
	player2->id = otherIs;

	if(Iam == 1)
	{
		player1->x = 0;
		player1->y = 0;
		player2->x = WIDTH-1;
		player2->y = HEIGHT-1;
	}
	else
	{
		player1->x = WIDTH-1;
		player1->y = HEIGHT-1;
		player2->x = 0;
		player2->y = 0;
	}

	// Reset Boxes
	memset(bombs,0,sizeof(bombs));
	memset(bombeffects,0,sizeof(bombeffects));
}